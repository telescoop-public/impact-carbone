module.exports = {
  root: true,
  env: {
    node: true,
    // avoid eslint errors on tests files 
    "jest": true
  },
  extends: [
    "plugin:vue/essential",
    "eslint:recommended"
  ],
  parserOptions: {
    parser: "babel-eslint"
  },
  rules: {
    'no-debugger': process.env.NODE_ENV === "development" ? "off" : "error",
    'no-console': "off"
  }
}
