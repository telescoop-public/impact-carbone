import {transportParser, formatTransportResult, isPossibleTransportStart} from "./parse_transport";

// test full parsing
test('parses 5km a pied', () => {
  expect(transportParser('5km a pied')).toEqual({
    distance: '5', unit: 'km', means: 'pied', preposition: 'a'
  });
});

test('parses 123km en avion', () => {
  expect(transportParser('123km en avion')).toEqual({
    distance: '123', unit: 'km', means: 'avion', preposition: 'en'
  });
});

test('parses 5 metres en train', () => {
  expect(transportParser('5 metres en train')).toEqual({
    distance: '5', unit: 'metres', means: 'train', preposition: 'en'
  });
});

// test start of text
test('start of 322.4 met', () => {
  expect(isPossibleTransportStart('322.4 met')).toEqual({
    distance: '322.4', unit: 'metre', means: 'voiture', preposition: 'en'
  })
});

test('start of 22 km en', () => {
  expect(isPossibleTransportStart('22 km en')).toEqual({
    distance: '22', unit: 'km', means: 'voiture', preposition: 'en'
  })
});

test('start of 3.24 m a', () => {
  expect(isPossibleTransportStart('3.24 m a')).toEqual({
    distance: '3.24', unit: 'm', means: 'velo', preposition: 'a'
  })
});

// test formatting result
test("formatting {distance: '3.24', unit: 'm', means: 'velo', preposition: 'a'}", () => {
  expect(formatTransportResult(
    {distance: '3.24', unit: 'm', means: 'velo', preposition: 'a'}
    )).toEqual('3.24 m à vélo')
});

test("formatting {distance: '5', unit: 'km', means: 'avion', preposition: 'en'}", () => {
  expect(formatTransportResult(
    {distance: '5', unit: 'km', means: 'avion', preposition: 'en'}
    )).toEqual('5 km en avion')
});
