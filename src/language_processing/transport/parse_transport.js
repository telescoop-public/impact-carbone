// module to parse transport input and suggest autocompletion
// transportParser(input) returns a structured search from input when valid
// isPossibleTransportStart(input) returns a structured suggestion if input
//   can be the start of a valid transport input
// formatTransportResult(structured_input) formats the structured input into plain language
const transport_means = ["voiture", "velo", "train", "pied", "avion"];
const units = ["km", "m", "metre", "metres", 'kilometre', "kilometres"];
const prepositions = ["en", "a"];

const space = '\\s+';
const optionalSpace = '\\s*';
const floatRegex = "[0-9]+(.[0-9]+)?";
const PRETTIFY = {
  "a": "à",
  "velo": "vélo",
  "metre": "mètre",
  "metres": "mètres",
  "kilometre": "kilomètre",
  "kilometres": "kilomètres",
};

const makeGroup = (name, possibilites, optional=false) => {
  let toReturn = `(?<${name}>${possibilites.join('|')})`;
  if (optional) {
    return toReturn + "?" + optionalSpace;
  }
  return toReturn;
};

const namedFloat = (name) => `(?<${name}>${floatRegex})`;

const beginningOfWord = (word, group_prefix) => {
  let beginning = '';
  const options = [];
  for (const letter of word) {
    beginning += letter;
    options.push(beginning);
  }
  return `(?<${group_prefix}__${word}>${options.join("|")})$`
};

const beginningOfWords = (groupPrefix, words) => {
  return "(" + words.map(word => beginningOfWord(word, groupPrefix)).join('|') + ")";
};

let regexStrings = [
  // 20 km en voiture
  namedFloat("distance")
  + optionalSpace
  + makeGroup("unit", units)
  + space
  + makeGroup("preposition", prepositions, true)
  + makeGroup("means", transport_means),
];

const regexes = regexStrings.map(regex => optionalSpace + regex + optionalSpace).map(regex => new RegExp(regex));

// longest regex first (we will stop at first one)
let possibleStartRegexStrings = [
  // 20 km en voit
  namedFloat("distance")
  + optionalSpace
  + makeGroup("unit", units)
  + space
  + makeGroup("preposition", prepositions, true)
  + space
  + beginningOfWords("means", transport_means),
  // 20 km e
  namedFloat("distance")
  + optionalSpace
  + makeGroup("unit", units)
  + space
  + beginningOfWords("preposition", prepositions),
  // 20 k
  namedFloat("distance")
  + optionalSpace
  + beginningOfWords("unit", units),
  // 20
  namedFloat("distance"),
];

let possibleStartRegexes = possibleStartRegexStrings.map(regex => optionalSpace + regex + optionalSpace).map(regex => new RegExp(regex));

const DEFAULT_TRANSPORT_VALUES = {
  "distance": 10,
  "unit": "km",
  "preposition": "en",
  "means": "voiture",
};

const transportResult = match => ({
  distance: match.groups.distance,
  unit: match.groups.unit,
  means: match.groups.means,
  preposition: match.groups.preposition,
});

// given the input, if a match is found, returns an object with keys
// distance, means, preposition
export const transportParser = (input) => {
  for (let regex of regexes) {
    let match = regex.exec(input);
    if (match) {
      return transportResult(match);
    }
  }

  return null;
};

const partialResult = match => {
  const toReturn = transportResult(match);
  for (let [groupKey, groupValue] of Object.entries(match.groups)) {
    if (groupKey.indexOf("__") !== -1 && groupValue){
      let [key, value] = groupKey.split("__");
      toReturn[key] = value;
    }
  }

  return toReturn;
};

// given the input, if a match is found, returns a suggestion as an object
// distance, means, preposition
export const isPossibleTransportStart = (input) => {
  let toReturn = null;
  for (let regex of possibleStartRegexes) {
    let match = regex.exec(input);
    if (match) {
      toReturn = partialResult(match);
      break;
    }
  }

  if (!toReturn) {
    return null;
  }

  if (!toReturn['means'] && toReturn['preposition'] === 'a') {
    toReturn['means'] = 'velo';
  }

  for (let [key, value] of Object.entries(DEFAULT_TRANSPORT_VALUES)) {
    if (!toReturn[key]) {
      toReturn[key] = value;
    }
  }

  return toReturn;
};

const prettify = word => PRETTIFY[word] || word;

export const formatTransportResult = transportResult => {
  return (
    `${transportResult['distance']} ${prettify(transportResult['unit'])} `
    + `${prettify(transportResult['preposition'])} ${prettify(transportResult['means'])}`
  )
};
