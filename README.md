[FR]
Ce projet a été réalisé dans le cadre d'un atelier d'une demi-journée pendant lequel l'objectif était d'interroger une base de données sur les facteurs d'émissions nécessaire à la réalisation d'exercices de compatibilité carbone.

Selon le site data.gouv.fr, le facteur d'émission est un ratio qui permet de connaître les émissions de gaz à effets de serre liés à un objet, une matière, ou un service.

Selon l'ADEME, "Aujourd'hui, elle est la base de données de référence de l'article 75 de la loi Grenelle II et est entièrement homogène avec l'article L1341-3 du Code des Transports et les valeurs par défaut du système d'échange des quotas d'émissions européen.
Administrée par l'ADEME, sa gouvernance est multi acteurs : 14 membres la composent tels que le Ministère de la Transition écologique et solidaire (MTES), le Mouvement des entreprises de France (MEDEF), le Réseau Action Climat (RAC), l’Association des Professionels en Conseil Climat (APCC), etc. Son enrichissement est ouvert à tous via la possibilité de contributions externes".

Ce projet propose donc d'interroger cette base de données fournie par l'ADEME, avec des mots-clés, pour connaître les émissions de gaz à effets de serre émis par les objets, matières ou services.

[EN]
This project was carried out as part of a half-day workshop where the objective was to be able to query a database of emission factors necessary to conduct carbon compatibility exercises.

According to the site data.gouv.fr, the emission factor is a ratio that makes it possible to know the greenhouse gas emissions linked to an object, material or service.

According to ADEME, "Today, it is the reference database for Article 75 of the Grenelle II Act and is fully consistent with Article L1341-3 of the Transport Code and the default values of the European Emissions Trading Scheme.
Administered by ADEME, its governance is multi-stakeholder: 14 members make it up, such as the Ministry of Ecological and Solidarity Transition (MTES), the Mouvement des entreprises de France (MEDEF), the Climate Action Network (RAC), the Association des Professionels en Conseil Climat (APCC), etc. Its enrichment is open to all via the possibility of external contributions".

This project proposes to query this database provided by ADEME, using keywords to find out the greenhouse gas emissions emitted by objects, materials or services.

Reference:
Base Carbone complète de l'ADEME in french, v.17, https://www.data.gouv.fr/fr/datasets/base-carbone-complete-de-lademe-en-francais-v17-0/#_

[About]

This project is made with Vue.JS as the front-end framework and the library fuse.js to finds the strings that are approximately equal to a given pattern.

Feel free to contribute.
